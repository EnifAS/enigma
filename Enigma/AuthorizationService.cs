﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;

namespace Enigma
{
    public class AuthorizationService
    {
        private const string UserData = "\\UserData.txt";

        private readonly string _mainFolder;

        public string UserName { get; private set; }

        public AuthorizationService(string path)
        {
            _mainFolder = path;
        }

        public bool IsAccountExist()
        {
            Console.Clear();
            Console.WriteLine("Do you already have an account?\n1 - yes\n2 - no");
            var wayToStartUseApp = Console.ReadKey(true);

            while (wayToStartUseApp.KeyChar != '1' && wayToStartUseApp.KeyChar != '2')
            {
                Console.Clear();
                Console.WriteLine("Incorrect input. Try again. If you already have an account press \"1\", otherwise press \"2\".");
                wayToStartUseApp = Console.ReadKey();
            }

            Console.Clear();

            try
            {
                if (wayToStartUseApp.KeyChar == '1' && Directory.GetDirectories(_mainFolder).Length == 0)
                {
                    if (Directory.GetDirectories(_mainFolder).Length == 0)
                    {
                        Console.WriteLine("You are the first user in the web! Please, create an account.");
                    }

                    return false;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            if (wayToStartUseApp.KeyChar == '2')
            {
                return false;
            }

            return true;
        }

        public bool CreateNewAccount(string pathToAppDirectory)
        {
            var isNameCorrect = false;
            var pathToUserAccaunt = string.Empty;
            Console.Write("Enter username (don't use \\/:*?\"<>|): ");
            string name = string.Empty;

            while (!isNameCorrect)
            {
                try
                {
                    name = CatchIncorrectName(pathToAppDirectory);

                    if (name == null)
                    {
                        return false;
                    }

                    Console.Clear();
                    isNameCorrect = true;
                }
                catch
                {
                    isNameCorrect = false;
                }

                if (name != null && !isNameCorrect)
                {
                    Console.WriteLine("Incorrect input. Please, choose another name.");
                }
            }

            Console.Clear();
            Console.WriteLine($"Enter username (don't use \\/:*?\"<>|): {name}");
            Console.Write("Enter password (length can't be less than 5 characters): ");

            var password = Console.ReadLine();

            while (password.Length < 5)
            {
                var message = $"Incorrect password. Please, try arain.\nEnter username (don't use \\/:*?\"<>|): {name}\nEnter password (length can't be less than 5 characters): ";
                password = GetUserInput(message, true);

                if (password == null)
                {
                    return false;
                }
            }

            try
            {

                pathToUserAccaunt = $"{pathToAppDirectory}\\{name}";
                Directory.CreateDirectory(pathToUserAccaunt);

                var textFilePath = $"{pathToUserAccaunt}{UserData}";

                using (StreamWriter sw = File.CreateText(textFilePath))
                {
                    sw.WriteLine(GetMD5Encrypt(name));
                    sw.WriteLine(GetMD5Encrypt(password));
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            return true;
        }

        public void Login(string pathToAppDirectory)
        {
            var nameMessage = "Welcome! Please log in.\n" + "Enter username: ";
            var name = GetUserInput(nameMessage, true);

            if (name == null)
            {
                UserName = null;
                return;
            }

            var passwordMessage = "Enter password: ";
            var password = GetUserInput(passwordMessage, false);

            while (!Directory.Exists($"{pathToAppDirectory}\\{name}") || !CheckPassword(pathToAppDirectory, name, password))
            {
                if (name.Length == 0 || !Directory.Exists($"{pathToAppDirectory}\\{name}"))
                {
                    nameMessage = "User with this username doesn't exist. Please, try again. (If you want to return, press \"Escape\")\n" + "Enter username: ";
                }
                else
                {
                    nameMessage = "Incorrect password. Please, try again.\n" + "Enter username: ";

                }

                name = GetUserInput(nameMessage, true);

                if (name == null)
                {
                    UserName = null;
                    return;
                }

                passwordMessage = "Enter password: ";
                password = GetUserInput(passwordMessage, false);

                if (password == null)
                {
                    UserName = null;
                    return;
                }
            }

            UserName = name;
        }

        private string CatchIncorrectName(string mainFolder)
        {
            var pattern = "[/\\\\:*?\"<>|]";
            var name = Console.ReadLine();

            while (Regex.IsMatch(name, pattern) || Directory.Exists($"{mainFolder}\\{name}") || name.Length == 0)
            {
                string message;

                if (Regex.IsMatch(name, pattern))
                {
                    message = "You can't use \\/:*?\"<>|. Please, try again. (If you want to return, press \"Escape\")\n" + "Enter username: ";
                }
                else if (name.Length != 0 && Directory.Exists($"{mainFolder}\\{name}"))
                {
                    message = $"Name \"{name}\" already exists. Please, try again. (If you want to return, press \"Escape\")\n" + "Enter username (don't use \\/:*?\"<>|): ";
                }
                else
                {
                    message = $"Name must contain at least one letter. Please, try again. (If you want to return, press \"Escape\")\n" + "Enter username (don't use \\/:*?\"<>|): ";
                }

                name = GetUserInput(message, true);

                if (name == null)
                {
                    break;
                }
            }

            return name;
        }

        private string GetUserInput(string message, bool needToUpdateInfo)
        {
            if (needToUpdateInfo)
            {
                Console.Clear();
            }

            Console.Write(message);

            var checkFirstInput = Console.ReadKey(true);

            if (checkFirstInput.Key == ConsoleKey.Escape)
            {
                Console.WriteLine();
                return null;
            }
            else if (checkFirstInput.Key == ConsoleKey.Enter)
            {
                Console.WriteLine();
                return string.Empty;
            }

            Console.Write(checkFirstInput.KeyChar.ToString());
            string input = checkFirstInput.KeyChar.ToString() + Console.ReadLine();

            return input;
        }

        private bool CheckPassword(string mainDirectory, string username, string password)
        {
            if (password == null || username.Length == 0 || password.Length == 0)
            {
                return false;
            }

            try
            {
                var path = $"{mainDirectory}\\{username}{UserData}";
                var allDataInFile = File.ReadAllLines(path);
                var passwordInFile = allDataInFile[1];

                if (passwordInFile == GetMD5Encrypt(password))
                {
                    return true;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            return false;
        }

        private string GetMD5Encrypt(string input)
        {
            MD5 md5Hasher = MD5.Create();
            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input));
            var sBuilder = new StringBuilder();

            foreach (var element in data)
            {
                sBuilder.Append(element.ToString("x2"));
            }

            return sBuilder.ToString();
        }
    }
}
