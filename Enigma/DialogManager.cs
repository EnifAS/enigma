﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Enigma
{
    public class DialogManager
    {
        private string _mainDirectory;

        private string _userFolder;

        private string _username;

        private Encryption _encryption;

        private Dictionary<int, string> _contacts = new();

        public DialogManager(string mainDirectory, string username)
        {
            _mainDirectory = mainDirectory;
            _username = username;
            _userFolder = $"{mainDirectory}\\{username}";
            _encryption = new Encryption();
        }

        public void StartDialog()
        {
            var userChoice = '\0';

            do
            {
                Console.Clear();
                ListOfContacts();
                userChoice = ChooseAction();

                try
                {
                    if (userChoice == '1')
                    {
                        SendingMessage();
                    }

                    if (userChoice == '2')
                    {
                        RemovingItems();
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }

            } while (userChoice != '3');
        }

        private void SendingMessage()
        {
            char action = UpdateInfo("Select the required action. Press:\n1 - choose one of your contacts\n2 - add new contact\n3 - to return");

            var nameNumber = 0;

            if (action == '1' && Directory.GetDirectories(_userFolder).Length != 0)
            {
                nameNumber = CheckDirectory("Enter your contact's number.");

                if (nameNumber == 0)
                {
                    return;
                }
            }

            if ((action == '1' && Directory.GetDirectories(_userFolder).Length == 0) || action == '2')
            {
                Console.Clear();
                ListOfContacts();

                if (action == '1' && Directory.GetDirectories(_userFolder).Length == 0)
                {
                    Console.WriteLine("You haven't any contacts. Please add it. Enter username for search. Press \"Escape\" to return.");
                }
                else
                {
                    Console.WriteLine("Enter name of user who you want to add to your contacts. Press \"Escape\" to return.");
                }

                string name = AddContact();

                if (name is null)
                {
                    return;
                }

                nameNumber = _contacts.FirstOrDefault(x => x.Value == name).Key;
            }

            if (action != '3')
            {
                WriteMessage(nameNumber);
                Console.Clear();
                OpenChat(_contacts[nameNumber]);
                Console.WriteLine("Press \"Enter\" to continue.");
                Console.ReadLine();
            }
        }

        private void RemovingItems()
        {
            Console.Clear();
            ListOfContacts();
            Console.WriteLine("Select the required action. Press:");
            Console.WriteLine("1 - delete a chat");
            Console.WriteLine("2 - delete a message");
            Console.WriteLine("3 - return");

            char userAction = Console.ReadKey().KeyChar;

            while (userAction != '1' && userAction != '2' && userAction != '3')
            {
                Console.WriteLine("\nIncorrect input. Try again.");
                userAction = Console.ReadKey().KeyChar;
            }

            if (userAction != '3' && Directory.GetDirectories(_userFolder).Length == 0)
            {
                Console.WriteLine("\nYou haven't any contacts. Press \"Enter\" to continue.");
                Console.ReadKey();
                return;
            }

            if (userAction == '1')
            {
                int contactNumber = CheckDirectory("Enter the number of the contact you want to delete or press \"Escape\" return .");

                if (contactNumber == 0)
                {
                    return;
                }

                DeleteChat(_contacts[contactNumber]);
            }
            else if (userAction == '2')
            {
                int contactNumber = CheckDirectory("Enter the number of the contact to delete a message. If you want to return press \"Escape\".");

                if (contactNumber == 0)
                {
                    return;
                }

                DeleteMessege(_contacts[contactNumber]);
                Console.Clear();
                OpenChat(_contacts[contactNumber]);
                Console.WriteLine("Press \"Enter\" to continue.");
                Console.ReadKey();
            }
        }

        private string AddContact()
        {
            string newContactName = GetInput();

            if (newContactName == null)
            {
                return null;
            }

            try
            {
                while (newContactName.Length == 0 || _userFolder == ($"{_mainDirectory}\\{newContactName}") || !Directory.Exists($"{_mainDirectory}\\{newContactName}"))
                {
                    Console.Clear();
                    ListOfContacts();
                    Console.WriteLine("Incorrect input. Try again or press \"Escape\" to return.");

                    newContactName = GetInput();

                    if (newContactName == null)
                    {
                        return null;
                    }
                }

                var pathToUsersContact = $"{_userFolder}\\{newContactName}";
                Directory.CreateDirectory(pathToUsersContact);
                Directory.CreateDirectory($"{pathToUsersContact}\\Incoming");
                Directory.CreateDirectory($"{pathToUsersContact}\\Outgoing");

                var pathToContact = $"{_mainDirectory}\\{newContactName}\\{_username}";
                Directory.CreateDirectory($"{pathToContact}\\Incoming");
                Directory.CreateDirectory($"{pathToContact}\\Outgoing");

                _encryption.GenerateShift(_mainDirectory, _username, newContactName);

                var listOfContacts = new List<string>(Directory.EnumerateDirectories(_userFolder));
                var counter = 1;
                _contacts.Clear();

                listOfContacts.ForEach(contact =>
                {
                    var directoryName = contact[(contact.LastIndexOf(Path.DirectorySeparatorChar) + 1)..];

                    if (_contacts.ContainsKey(counter) && _contacts[counter] != directoryName)
                    {
                        _contacts[counter] = directoryName;
                    }
                    else if (!_contacts.ContainsKey(counter))
                    {
                        _contacts.Add(counter, contact[(contact.LastIndexOf(Path.DirectorySeparatorChar) + 1)..]);
                    }

                    counter++;
                });
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            return newContactName;
        }

        private string GetInput()
        {
            var checkFirstInput = Console.ReadKey(true);

            if (checkFirstInput.Key == ConsoleKey.Escape)
            {
                Console.WriteLine();
                return null;
            }

            Console.Write(checkFirstInput.KeyChar.ToString());
            var newContactName = checkFirstInput.KeyChar.ToString() + Console.ReadLine();

            return newContactName;
        }

        private void WriteMessage(int contactName)
        {
            OpenChat(_contacts[contactName]);

            Console.WriteLine();
            Console.Write("   Enter your message:");
            Console.Write("  ");
            var message = Console.ReadLine();

            try
            {
                var keyPath = $"{_userFolder}\\{_contacts[contactName]}";
                var allDataInFile = File.ReadAllLines(Directory.GetFiles(keyPath)[0]);
                var key = Convert.ToByte(allDataInFile[0]);
                var encryptedMessage = _encryption.EncryptMessage(message, key);

                var pathToOutgoing = $"{_userFolder}\\{_contacts[contactName]}\\Outgoing";
                var outgoingInfo = new DirectoryInfo(pathToOutgoing);
                var outgoingFiles = outgoingInfo.GetFiles().OrderBy(i => i.CreationTime).ToList();
                var lastOutgoing = outgoingFiles.Count == 0 ? null : outgoingFiles[^1];
                var lastOutgoingName = Path.GetFileName(lastOutgoing?.FullName) ?? "0";

                var pathToIncoming = $"{_userFolder}\\{_contacts[contactName]}\\Incoming";
                var incomingInfo = new DirectoryInfo(pathToIncoming);
                var incomingFiles = incomingInfo.GetFiles().OrderBy(i => i.CreationTime).ToList();
                var lastIncoming = incomingFiles.Count == 0 ? null : incomingFiles[^1];
                var lastIncomingName = Path.GetFileName(lastIncoming?.FullName) ?? "0";

                var nameOfNewFile = string.Empty;

                if (lastOutgoingName == "0" && lastIncomingName == "0")
                {
                    nameOfNewFile = "1";
                }
                else if (Convert.ToInt32(lastOutgoingName) < Convert.ToInt32(lastIncomingName))
                {
                    nameOfNewFile = (Convert.ToInt32(lastIncomingName) + 1).ToString();
                }
                else
                {
                    nameOfNewFile = (Convert.ToInt32(lastOutgoingName) + 1).ToString();
                }

                var contactFolder = $"{_mainDirectory}\\{_contacts[contactName]}\\{_username}\\Incoming";

                using (StreamWriter userFile = File.CreateText($"{pathToOutgoing}\\{nameOfNewFile}"))
                {
                    userFile.WriteLine(encryptedMessage);
                }

                using (StreamWriter contactFile = File.CreateText($"{contactFolder}\\{nameOfNewFile}"))
                {
                    contactFile.WriteLine(encryptedMessage);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private void DeleteChat(string contact)
        {
            try
            {
                var pathToUserDir = $"{_userFolder}\\{contact}";
                DeleteDirectories(pathToUserDir);
                Directory.Delete(pathToUserDir);

                var pathToContactDir = $"{_mainDirectory}\\{contact}\\{_username}";
                DeleteDirectories(pathToContactDir);
                Directory.Delete(pathToContactDir);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private void DeleteDirectories(string path)
        {
            var fileEntries = Directory.GetFiles(path);

            for (int i = 0; i < fileEntries.Length; i++)
            {
                File.Delete(fileEntries[i]);
            }

            var subdirectoryEntries = Directory.GetDirectories(path);

            for (int i = 0; i < subdirectoryEntries.Length; i++)
            {
                DeleteDirectories(subdirectoryEntries[i]);
                Directory.Delete(subdirectoryEntries[i]);
            }
        }

        private void DeleteMessege(string contactName)
        {
            Console.Clear();
            OpenChat(contactName);

            try
            {
                var userIncomingPath = $"{_userFolder}\\{contactName}\\Incoming";
                var userOutgoingPath = $"{_userFolder}\\{contactName}\\Outgoing";
                var contactIncomingPath = $"{_mainDirectory}\\{contactName}\\{_username}\\Incoming";
                var contactOutgoingPath = $"{_mainDirectory}\\{contactName}\\{_username}\\Outgoing";

                var userIncoming = new DirectoryInfo(userIncomingPath);
                var allUserIncomingFiles = userIncoming.GetFiles().OrderBy(i => i.CreationTime).ToList();

                var userOutgoing = new DirectoryInfo(userOutgoingPath);
                var allUserOutgoingFiles = userOutgoing.GetFiles().OrderBy(i => i.CreationTime).ToList();

                var contactIncoming = new DirectoryInfo(contactIncomingPath);
                var allContactIncomingFiles = contactIncoming.GetFiles().OrderBy(i => i.CreationTime).ToList();

                var contactOutgoing = new DirectoryInfo(contactOutgoingPath);
                var allContactOutgoingFiles = contactOutgoing.GetFiles().OrderBy(i => i.CreationTime).ToList();

                Console.Write("Choose number of message you want to delete:");
                var messageNumber = Console.ReadLine();

                var isIncomingContains = new FileInfo($"{userIncomingPath}\\{messageNumber}").Exists;
                var isOutgoingContains = new FileInfo($"{userOutgoingPath}\\{messageNumber}").Exists;

                while (!isIncomingContains && !isOutgoingContains)
                {
                    Console.WriteLine("Incorrect input.Try again.");
                    messageNumber = Console.ReadLine();
                }

                var fileNumber = Convert.ToInt32(messageNumber);

                if (isIncomingContains)
                {
                    File.Delete($"{userIncomingPath}\\{messageNumber}");
                    File.Delete($"{contactOutgoingPath}\\{messageNumber}");
                }
                else
                {
                    File.Delete($"{userOutgoingPath}\\{messageNumber}");
                    File.Delete($"{contactIncomingPath}\\{messageNumber}");
                }

                RenamingFiles(fileNumber, allUserIncomingFiles, allContactOutgoingFiles, userIncomingPath, contactOutgoingPath);
                RenamingFiles(fileNumber, allContactIncomingFiles, allUserOutgoingFiles, contactIncomingPath, userOutgoingPath);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private void RenamingFiles(int fileNumber, List<FileInfo> incomingList, List<FileInfo> outgoingList, string incomingPath, string outgoingPath)
        {
            for (int i = 0; i < incomingList.Count; i++)
            {
                var currentUserFile = Path.GetFileName(incomingList[i].FullName);
                var currentContactFile = Path.GetFileName(outgoingList[i].FullName);

                if (Convert.ToInt32(currentUserFile) > fileNumber)
                {
                    var newNumber = Convert.ToInt32(currentUserFile) - 1;
                    File.Move($"{incomingPath}\\{currentUserFile}", $"{incomingPath}\\{newNumber}");
                    File.Move($"{outgoingPath}\\{currentContactFile}", $"{outgoingPath}\\{newNumber}");
                }
            }
        }

        private char ChooseAction()
        {
            Console.WriteLine("Select the required action. Press:");
            Console.WriteLine("1 - start a chat");
            Console.WriteLine("2 - delete a chat or message");
            Console.WriteLine("3 - logout");
            var userAction = Console.ReadKey().KeyChar;

            while (userAction != '1' && userAction != '2' && userAction != '3')
            {
                Console.WriteLine("\nIncorrect input. Try again.");
                userAction = Console.ReadKey().KeyChar;
            }

            return userAction;
        }

        private void ListOfContacts()
        {
            Console.WriteLine("Your contacts:");

            try
            {
                var directories = new List<string>(Directory.EnumerateDirectories(_userFolder));
                var counter = 1;
                _contacts.Clear();

                directories.ForEach(directory =>
                {
                    var directoryName = directory[(directory.LastIndexOf(Path.DirectorySeparatorChar) + 1)..];

                    if (_contacts.ContainsKey(counter) && _contacts[counter] != directoryName)
                    {
                        _contacts[counter] = directoryName;
                    }
                    else if (!_contacts.ContainsKey(counter))
                    {
                        _contacts.Add(counter, directory[(directory.LastIndexOf(Path.DirectorySeparatorChar) + 1)..]);
                    }

                    Console.Write($"{counter} - ");
                    Console.WriteLine($"{_contacts[counter]}");
                    counter++;
                });
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            Console.WriteLine("=========================================");
        }

        private void OpenChat(string contactName)
        {
            Console.Clear();
            ListOfContacts();

            Console.WriteLine($"           Chat with {contactName}");
            Console.WriteLine("-----------------------------------------");
            Console.WriteLine("     incoming           outgoing");
            Console.WriteLine();

            try
            {
                var incoming = new List<string>(Directory.EnumerateFiles($"{_userFolder}\\{contactName}\\Incoming"));
                var outgoing = new List<string>(Directory.EnumerateFiles($"{_userFolder}\\{contactName}\\Outgoing"));
                var keyPath = $"{_userFolder}\\{contactName}";
                var allDataInFile = File.ReadAllLines(Directory.GetFiles(keyPath)[0]);
                var key = Convert.ToByte(allDataInFile[0]);

                if (incoming.Count == 0 && outgoing.Count != 0)
                {
                    for (int i = 0; i < outgoing.Count; i++)
                    {
                        ReadMessages(outgoing[i], key, false);
                    }
                }
                else if (incoming.Count != 0 && outgoing.Count == 0)
                {
                    for (int i = 0; i < incoming.Count; i++)
                    {
                        ReadMessages(incoming[i], key, true);
                    }
                }
                else
                {
                    for (int i = 1; i <= (incoming.Count + outgoing.Count); i++)
                    {
                        var isIncomingContains = incoming.Contains($"{_userFolder}\\{contactName}\\Incoming\\{i}");
                        var pathToMessage = $"{_userFolder}\\{contactName}\\";

                        if (isIncomingContains)
                        {
                            pathToMessage += $"Incoming\\{i}";
                            ReadMessages(pathToMessage, key, true);
                        }
                        else
                        {
                            pathToMessage += $"Outgoing\\{i}";
                            ReadMessages(pathToMessage, key, false);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private char UpdateInfo(string message)
        {
            Console.Clear();
            ListOfContacts();
            Console.WriteLine(message);
            var input = Console.ReadKey().KeyChar;

            while (input != '1' && input != '2' && input != '3')
            {
                Console.WriteLine("\nIncorrect input. Try again.");
                input = Console.ReadKey().KeyChar;
            }

            return input;
        }

        private int CheckDirectory(string message)
        {
            Console.Clear();
            ListOfContacts();
            Console.WriteLine(message);
            string input = string.Empty;

            input = GetInput();

            if (input == null)
            {
                return 0;
            }

            while (input.Length == 0 || !input.All(char.IsDigit) || !_contacts.ContainsKey(Convert.ToInt32(input)))
            {
                Console.WriteLine("You haven't such contact. Try again.");
                input = GetInput();

                if (input == null)
                {
                    return 0;
                }
            }

            return Convert.ToInt32(input);
        }

        private void ReadMessages(string message, byte key, bool isIncoming)
        {
            try
            {
                var numberOfMessage = Path.GetFileName(message);
                var textFromFile = File.ReadAllText(message);
                var messageDecryption = _encryption.DecryptionMessage(textFromFile[..^2], key);
                var countOfSpaces = string.Empty;
                var lengthSpaces = 4 - numberOfMessage.Length;

                while (lengthSpaces > 0)
                {
                    countOfSpaces += " ";
                    lengthSpaces--;
                }

                if (isIncoming)
                {
                    Console.Write($"{countOfSpaces}{numberOfMessage} ");
                    Console.Write($"{messageDecryption}", Console.ForegroundColor = ConsoleColor.Cyan);
                }
                else
                {
                    Console.Write($"{countOfSpaces}{numberOfMessage}\t\t\t");
                    Console.Write($"{messageDecryption}", Console.ForegroundColor = ConsoleColor.Yellow);
                }

                Console.ResetColor();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
