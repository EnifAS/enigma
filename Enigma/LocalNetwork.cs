﻿namespace Enigma
{
    public class LocalNetwork
    {
        public string MainDirectory { get; private set; }

        public LocalNetwork()
        {
            MainDirectory = "E:\\001\\EmigmaMessanger";
        }

        public void TurnOnTheApp()
        {
            var authorization = new AuthorizationService(MainDirectory);

            while (true)
            {
                bool isAccountCreated = true;

                if (!authorization.IsAccountExist())
                {
                    isAccountCreated = authorization.CreateNewAccount(MainDirectory);
                }

                if (isAccountCreated)
                {
                    authorization.Login(MainDirectory);
                }
                
                if (authorization.UserName != null)
                {
                    var dialog = new DialogManager(MainDirectory, authorization.UserName);
                    dialog.StartDialog();
                }
            }
        }
    }
}
