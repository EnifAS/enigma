﻿using System;

namespace Enigma
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                new LocalNetwork().TurnOnTheApp();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
            }
        }
    }
}
