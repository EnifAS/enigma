﻿using System;
using System.IO;
using System.Text;

namespace Enigma
{
    public class Encryption
    {
        public void GenerateShift(string mainFolder, string userName, string contactName)
        {
            var random = new Random();
            var shift = random.Next(1, 215);

            var keyFile = "\\Key.txt";
            var pathToUser = $"{mainFolder}\\{userName}\\{contactName}";
            var pathToContact = $"{mainFolder}\\{contactName}\\{userName}";

            try
            {
                using (StreamWriter userFile = File.CreateText(pathToUser + keyFile))
                {
                    userFile.WriteLine(shift);
                }

                using (StreamWriter contactFile = File.CreateText(pathToContact + keyFile))
                {
                    contactFile.WriteLine(shift);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public string EncryptMessage(string message, byte shift)
        {
            var ascii = Encoding.ASCII;
            var unicode = Encoding.Unicode;

            byte[] asciiBytes = ascii.GetBytes(message);
            byte[] unicodeBytes = Encoding.Convert(ascii, unicode, asciiBytes);

            for (int i = 0; i < unicodeBytes.Length; i++)
            {
                unicodeBytes[i] += shift;
            }

            return Encoding.Unicode.GetString(unicodeBytes);
        }

        public string DecryptionMessage(string message, byte shift)
        {
            byte[] unicodeBytes = Encoding.Unicode.GetBytes(message);
            byte[] asciiBytes = new byte[unicodeBytes.Length / 2];

            for (byte i = 0, j = 0; i < message.Length * 2; i++)
            {
                if (i % 2 != 0)
                {
                    continue;
                }

                asciiBytes[j] = (byte)(unicodeBytes[i] - shift);
                j++;
            }

            return Encoding.ASCII.GetString(asciiBytes) + "\n";
        }
    }
}
